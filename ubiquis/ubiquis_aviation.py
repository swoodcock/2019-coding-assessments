#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import sqlite3 as sq
import pandas as pd
import numpy as np

from geopy.distance import great_circle

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt, QSortFilterProxyModel, QStringListModel
from PyQt5.QtWidgets import QWidget, QGridLayout, QPushButton, QApplication, \
QVBoxLayout, QLabel, QCompleter, QComboBox


# Build Interface

class ExtendedComboBox(QComboBox):
    def __init__(self, parent=None):
        super(ExtendedComboBox, self).__init__(parent)

        self.setFocusPolicy(Qt.StrongFocus)
        self.setEditable(True)

        # add a filter model to filter matching items
        self.pFilterModel = QSortFilterProxyModel(self)
        self.pFilterModel.setFilterCaseSensitivity(Qt.CaseInsensitive)
        self.pFilterModel.setSourceModel(self.model())

        # add a completer, which uses the filter model
        self.completer = QCompleter(self.pFilterModel, self)
        # always show all (filtered) completions
        self.completer.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
        self.setCompleter(self.completer)

        # connect signals (possibly with delay timer)
#        self.timer = QtCore.QTimer()
#        self.timer.setSingleShot(True)
#        self.timer.setInterval(300)
#        self.timer.timeout.connect(self.pFilterModel.setFilterFixedString(self.currentText()))
#        self.lineEdit().textEdited.connect(lambda: self.timer.start())
        self.lineEdit().textEdited.connect(self.pFilterModel.setFilterFixedString)
        self.completer.activated.connect(self.on_completer_activated)
        

    # on selection of an item from the completer, select the corresponding item from combobox 
    def on_completer_activated(self, text):
        if text:
            index = self.findText(text)
            self.setCurrentIndex(index)
            self.activated[str].emit(self.itemText(index))
            

    # on model change, update the models of the filter and completer as well 
    def setModel(self, model):
        super(ExtendedComboBox, self).setModel(model)
        self.pFilterModel.setSourceModel(model)
        self.completer.setModel(self.pFilterModel)


    # on model column change, update the model column of the filter and completer as well
    def setModelColumn(self, column):
        self.completer.setCompletionColumn(column)
        self.pFilterModel.setFilterKeyColumn(column)
        super(ExtendedComboBox, self).setModelColumn(column)    


class ProgramWindow(QWidget):
    def __init__(self):
        super().__init__()
        grid_layout = QGridLayout()
        self.setLayout(grid_layout)
        self.setWindowTitle('Ubquis Aviation')
            
        gui_fields=['AirlineCode', 'SourceAirportCode', 'DestinationAirportCode', \
                    'Codeshare', 'Stops', 'Equipment', 'SourceAirportName', \
                    'SourceCity', 'SourceCountry', 'DestAirportName', 'DestCity', \
                    'DestCountry', 'AirlineName', 'AirlineAlias', 'AirlineICAO', \
                    'AirlineCallsign', 'AirlineCountry', 'AirlineActive']
        
        conn = sq.connect('openflightsdb.sqlite3')
        conn.row_factory = lambda cursor, row: row[0]
        cur = conn.cursor()

        j = -1
        for x in range(6):
            for y in range(3):
                j += 1
                column = gui_fields[j]
                label = QLabel(column)
                values = cur.execute('''SELECT DISTINCT {} FROM combined'''.format(column)).fetchall()
                values = [''] + [str(e) for e in values]
                # Dyanamically create variables here using exec(string)
                dropdown = "self." + column
                exec(dropdown + "= ExtendedComboBox()")
                exec(dropdown + ".clear()")
                exec(dropdown + ".addItems(values)")
                
                #dropdown = ExtendedComboBox()
                #dropdown.clear()
                #dropdown.addItems(values)
                vbox = QVBoxLayout()
                vbox.addWidget(label)
                
                exec("vbox.addWidget(" + dropdown + ")")
                #vbox.addWidget(dropdown)
                
                grid_layout.addLayout(vbox, x, y)
                

        short_button = QPushButton("Calculate Shortest Route")
        self.short_result = QLabel("Result displayed here")
        self.short_result.setAlignment(Qt.AlignCenter)
        long_button = QPushButton("Calculate Longest Route")
        self.long_result = QLabel("Result displayed here")
        self.long_result.setAlignment(Qt.AlignCenter)
        grid_layout.addWidget(short_button, 0, 4)
        grid_layout.addWidget(self.short_result, 1, 4)
        grid_layout.addWidget(long_button, 3, 4)
        grid_layout.addWidget(self.long_result, 4, 4)
        
        # Close db connection
        cur.close()
        conn.close()

        short_button.clicked.connect(lambda: self.calculate_longshort(gui_fields, True))
        long_button.clicked.connect(lambda: self.calculate_longshort(gui_fields, False))
        
    def calculate_longshort(self, f, short):
        dd = {field: None for field in f}
        for field in dd:
            exec("if " + "self." + field + ".currentText(): dd[field] = " + \
                 "self." + field + ".currentText()")
        #dd = {key: "'" + value + "'" if key != value else value for key, value in dd.items()}            
        
        conn = sq.connect('openflightsdb.sqlite3')
        conn.text_factory = str
        cur = conn.cursor()

#        cur.execute('''SELECT SourceAirportName, SourceCountry, DestAirportName,
#                    DestCountry, AirlineName, MIN(CalcDist)
#                    FROM combined
#                    WHERE AirlineCode = {} AND SourceAirportCode = {} AND
#                    DestinationAirportCode = {} AND Codeshare = {} AND Stops 
#                    = {} AND Equipment = {} AND SourceAirportName = {} 
#                    AND SourceCity = {} AND SourceCountry = {} AND 
#                    DestAirportName = {} AND DestCity = {} AND DestCountry 
#                    = {} AND AirlineName = {} AND AirlineAlias = {} AND
#                    AirlineICAO = {} AND AirlineCallsign = {} AND
#                    AirlineCountry = {} AND AirlineActive = {}'''.format(dd[f[0]], \
#                    dd[f[1]], dd[f[2]], dd[f[3]], dd[f[4]], dd[f[5]], dd[f[6]], \
#                    dd[f[7]], dd[f[8]], dd[f[9]], dd[f[10]], dd[f[11]], dd[f[12]], \
#                    dd[f[13]], dd[f[14]], dd[f[15]], dd[f[16]], dd[f[17]]))

        dd = {field: dd[field] for field in dd if dd[field]}
        if short:
            sql = '''SELECT SourceAirportName, SourceCountry, DestAirportName,
                DestCountry, AirlineName, MIN(CalcDist)
                FROM combined'''
        else:
            sql = '''SELECT SourceAirportName, SourceCountry, DestAirportName,
                DestCountry, AirlineName, MAX(CalcDist)
                FROM combined'''
        if dd:
            sql = sql + " WHERE "
            add_args = [field + "=" + "'" + dd[field] + "'" for field in dd]
            sql = sql + " AND ".join(add_args)
        try:
            print("SQL Statement:" + '\n' + sql)
            cur.execute(sql)
            min_v = cur.fetchone()
            min_v = min_v[0] + ": " + min_v[1] + '\n' + min_v[2] + ": " + \
            min_v[3] + '\n' + min_v[4] + '\n' + str(round(min_v[5], 2)) + " km"
            if short: self.short_result.setText(min_v)
            else: self.long_result.setText(min_v)
        except: 
            if short: self.short_result.setText("No Match")
            else: self.long_result.setText("No Match")

        cur.close()
        conn.close()
      

# Download Data, insert into .sqlite3, combined & calculate distances
def generate_database(tables, columns):      
    sql_data = 'openflightsdb.sqlite3'
    conn = sq.connect(sql_data)
    cur = conn.cursor()
    print("Data Download Start")
    for table in tables:
        df = pd.read_csv("https://raw.githubusercontent.com/jpatokal/openflights/master/data/" \
                         + table + ".dat", names=columns[table])
        #df = pd.read_csv(table + ".csv", names=columns[table])
        df.replace(["\\n", "\\N"], np.nan, inplace=True)
        df.replace("^Unnamed.*$", np.nan, regex=True, inplace=True)
        df.to_sql(table, conn, if_exists='replace', index=False)
        #conn.commit()
    print("Data Download End")
    cur.execute('''DELETE FROM airlines WHERE `AirlineID` LIKE (?)''', ('%-1%',))
    
    cur.execute('''SELECT r.* , a1.Name, a1.City, a1.Country, a1.Latitude,
                a1.Longitude, a2.Name, a2.City, a2.Country, a2.Latitude,
                a2.Longitude, ar.*
                FROM routes as r
                LEFT JOIN airports AS a1 ON a1.AirportID = r.SourceID
                LEFT JOIN airports AS a2 ON a2.AirportID = r.DestinationID
                LEFT JOIN airlines AS ar ON ar.AirlineID = r.AirlineID''')
    
    field_names=['AirlineCode', 'AirlineID', 'SourceAirportCode', 'SourceID', \
                 'DestinationAirportCode', 'DestinationID', 'Codeshare', \
                 'Stops', 'Equipment', 'SourceAirportName', 'SourceCity', \
                 'SourceCountry', 'SourceLat', 'SourceLon', 'DestAirportName', \
                 'DestCity', 'DestCountry', 'DestLat', 'DestLon', \
                 'AirlineID2', 'AirlineName', 'AirlineAlias', 'AirlineIATA', \
                 'AirlineICAO', 'AirlineCallsign', 'AirlineCountry', 'AirlineActive']
                 
    combined_df = pd.DataFrame([r for r in cur.fetchall()], columns=field_names)
    combined_df = combined_df.dropna(axis=0, subset=['SourceLat', 'SourceLon', 'DestLat', 'DestLon'])
    # Remove entries where source airport = destination airport
    combined_df = combined_df[combined_df['SourceAirportCode'] != combined_df['DestinationAirportCode']]
    
    # Calculate distances using Haversine (Change to geopy.geodesic for flattened sphere)
    source = [(x, y) for x, y in zip(combined_df['SourceLat'], combined_df['SourceLon'])]
    dest = [(x, y) for x, y in zip(combined_df['DestLat'], combined_df['DestLon'])]
    calcdist = [great_circle(x, y).kilometers for x, y in zip(source, dest)]
    combined_df['CalcDist'] = calcdist
    
    combined_df.to_sql("combined", conn, if_exists='replace', index=False)
    
    conn.commit()
    cur.close()
    conn.close()
# Alt: native import without pandas
#from pathlib import Path
#db_name = Path('my.db').resolve()
#csv_file = Path('file.csv').resolve()
#result = subprocess.run(['sqlite3',
#                         str(db_name),
#                         '-cmd',
#                         '.mode csv',
#                         '.import '+str(csv_file).replace('\\','\\\\')
#                                 +' <table_name>'],
#                        capture_output=True)


if __name__ == '__main__':
    
    items = ['airports', 'airlines', 'routes']
    col_names={'airports':['AirportID', 'Name', 'City', 'Country', 'IATA', \
                           'ICAO', 'Latitude', 'Longitude', 'Altitude', 'Timezone', \
                           'DST', 'Tzdatabasetimezone', 'Type', 'Source'], \
                'airlines':['AirlineID', 'Name', 'Alias', 'IATA', 'ICAO', \
                            'Callsign', 'Country', 'Active'], \
                'routes':['Airline', 'AirlineID', 'SourceAirport', \
                          'SourceID', 'DestinationAirport', \
                          'DestinationID', 'Codeshare', 'Stops', \
                          'Equipment']}
    
    generate_database(items, col_names)
    app = QApplication(sys.argv)
    window = ProgramWindow()
    window.show()
    sys.exit(app.exec_())
