# Ubiquis Aviation

Short Python/SQL project to assess competency during interviews (job offer received).

Self contained program that downloads data from openflights.org, databases, and analyses.

Created with a GUI for calculating basic statistics from flight data:
- Longest / shortest routes
- Filter by airline, airport, plane model
- Arbitrary filter combination, e.g.:
a) Longest route out of any Chinese airport, served by non-Chinese airlines on Airbus planes (any model)
b) Longest route within the US (departs from and arrives at American airports)
c) Longest route served by either an Airbus A380, a Boeing 747 or a Boeing 787

Assessed on cross-platform capability, speed of data aggregation and loading, speed of statistic computation.


**Notes on Haversine vs Geodesic distance calculations**:

- The Haversine formula calculates distances between points on a sphere (the great-circle distance), as does geopy.distance.great_circle.
- On the other hand, geopy.distance.geodesic calculates distances between points on an ellipsoidal model of the earth, which you can think of as a "flattened" sphere.
- The difference isn't due to rounding but the use different formulas instead, with the geodesic formula more accurately modeling the true shape of the earth.

**Initial manual Haversine formula**:

```
def Haversine(lat1,lon1,lat2,lon2, **kwarg):
    """
    This uses the ‘haversine’ formula to calculate the great-circle distance between two points – that is, 
    the shortest distance over the earth’s surface – giving an ‘as-the-crow-flies’ distance between the points.
    Haversine formula:
    a = sin²(Δφ/2) + cos φ1 ⋅ cos φ2 ⋅ sin²(Δλ/2)
    c = 2 ⋅ atan2( √a, √(1−a) )
    d = R ⋅ c
    where   φ is latitude, λ is longitude, R is earth’s radius (mean radius = 6,371km);
    note that angles need to be in radians to pass to trig functions!
    """
    R = 6371.0088
    lat1,lon1,lat2,lon2 = map(np.radians, [lat1,lon1,lat2,lon2])

    dlat = lat2 - lat1
    dlon = lon2 - lon1
    a = np.sin(dlat/2)**2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon/2) **2
    c = 2 * np.arctan2(a**0.5, (1-a)**0.5)
    d = R * c
    return round(d,4)
```

**Updated geodesic (geopy lib)**:
```
from geopy.distance import geodesic

origin = (30.172705, 31.526725)  # (latitude, longitude)
dist = (30.288281, 31.732326)

print(geodesic(origin, dist).meters)  # 23576.805481751613
print(geodesic(origin, dist).kilometers)  # 23.576805481751613
```
